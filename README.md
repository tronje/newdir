# newdir
A friendly tmp-dir script

Use it in your `.bashrc` or `.zshrc` like this:

```bash
alias newdir='eval $(/path/to/newdir.py)'
```
